# Tees on War [CLIENT 🎮]

## Stack
- Godot v3.2
- Git LFS 2.9.2

## Download the project
```
git clone https://gitlab.com/Juankz/tees-on-war-client.git
git lfs fetch
git lfs checkout
```
