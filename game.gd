extends Node

var Player

var world = null
var players
var player_info

func _ready():
	rpc("request_world_creation")
	
puppet func create_world(path):	
	world = load(path).instance()
	add_child(world)
	
	rpc("request_player_creation", player_info)
	
puppet func create_players(players):
	for key in players.keys():
		if not self.players.has(key):
			create_player(players[key])

func create_player(player_info):
	var player = Player.instance()
	player.name = str(player_info.id)
	world.get_node("Players").add_child(player)
	players[player_info.id] = player_info
	print(player_info.id)

puppet func remove_player(id):
	world.get_node("Players").get_node(str(id)).queue_free()
	players.erase(id)

func init_player(player_info):
	self.player_info = player_info
	players = {}
	Player = load("res://Players/Player.tscn")
