extends Spatial

onready var head = $ArmatureBody/Skeleton/Head as BoneAttachment
onready var shoes = $ArmatureBody/Skeleton/Shoes as MeshInstance

func change_costume(new_costume: Node, new_shoes: Mesh):
	if head.get_child_count() > 0:
		head.get_child(0).queue_free()
	head.add_child(new_costume)
	shoes.mesh = new_shoes
