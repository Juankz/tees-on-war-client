tool
extends EditorScenePostImport

var material = load('res://Players/Character/ColorAtlas_Mat.material')

func post_import(scene):
	scene.translation.y = -0.7
	apply_material(scene)
	return scene

func apply_material(node: Node):
	if node is MeshInstance:
		node.set_surface_material(0,material)
	if node.get_child_count() > 0:
		for child_node in node.get_children():
			apply_material(child_node)
