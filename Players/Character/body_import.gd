tool
extends EditorScenePostImport

func post_import(scene):
	# Merge ArmatureAction and KeyAction animations
	var ap = scene.get_node("AnimationPlayer") as AnimationPlayer
	var list = ap.get_animation_list() as PoolStringArray
	for anim in list:
		if anim.begins_with("ArmatureAction"):
			print(anim)
			var clip_name = anim.lstrip("ArmatureAction")
			var key_action_anim = ap.get_animation("KeyAction"+clip_name)
			var armature_anim = ap.get_animation(anim) as Animation
			add_anim_tracks(key_action_anim, armature_anim)
			ap.rename_animation(anim,clip_name)
	for anim in list:
		if anim.begins_with("KeyAction"):
			ap.remove_animation(anim)
	return scene

func add_anim_tracks(from: Animation, to: Animation) -> void:
	for track_idx in range(from.get_track_count()):
		from.copy_track(track_idx, to)
