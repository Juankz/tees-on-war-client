extends Node

const DEFAULT_PORT = 10567
signal successful_connection 

func _ready():
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("connection_failed", self, "_connection_failed")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _connected_to_server():
	print("_connection_successful")
	emit_signal("successful_connection")

func _connection_failed():
	print("_connection_failed")
	
func _server_disconnected():
	print("_server_disconnected")

func join_server(ip):
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(host)
