extends Control

var Game = load("res://Game.tscn")

var player = {}

func _ready():
	network_manager.connect("successful_connection", self, "create_game")

func join_server():
	var ip = $PanelContainer/GridContainer/IpInput.text
	network_manager.join_server(ip)

func create_game():
	player.name = "Tee"
	player.character_details = "punk"
	player.id = get_tree().get_network_unique_id()
	var game = Game.instance()
	game.init_player(player)
	get_tree().root.add_child(game)
	queue_free() #remove lobby
	
