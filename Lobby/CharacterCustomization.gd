extends Spatial

const DEFAULT_COSTUME = "Punk"

var costumes = {	
	"Jason": "res://Players/Character/Costumes/Jason.glb",
	"Punk": "res://Players/Character/Costumes/Punk.glb",
	"Roman": "res://Players/Character/Costumes/Roman.glb",
	"Viking": "res://Players/Character/Costumes/Viking.glb"
}

var shoes = {
	"Jason": "res://Players/Character/Shoes/JasonShoes.tres",
	"Roman": "res://Players/Character/Shoes/RomanShoes.tres",
	"Punk": "res://Players/Character/Shoes/SportShoes.mesh",
	"Viking": "res://Players/Character/Shoes/VikingShoes.tres"
}

var loaded_costumes = {}
var loaded_shoes = {}

func _ready():
	#change_costum(DEFAULT_COSTUME)
	$PanelContainer/VBoxContainer/Punk.connect("pressed", self, "change_costum", ["Punk"])
	$PanelContainer/VBoxContainer/Jason.connect("pressed", self, "change_costum", ["Jason"])
	$PanelContainer/VBoxContainer/Roman.connect("pressed", self, "change_costum", ["Roman"])
	$PanelContainer/VBoxContainer/Viking.connect("pressed", self, "change_costum", ["Viking"])

func change_costum(style):
	var costume
	var shoes
	
	if loaded_costumes.has(style): 
		costume = loaded_costumes[style]
	else:
		costume = load(costumes[style])
		loaded_costumes[style] = costume
	
	if loaded_shoes.has(style): 
		shoes = loaded_shoes[style]
	else:
		shoes = load(self.shoes[style])
		loaded_shoes[style] = shoes
		
	$Body.change_costume(costume.instance(), shoes)
